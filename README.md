# Demo Gitlab CI
## Pre-requisites
* IntelliJ IDEA
* Java JDK
* Gradle

## IntelliJ Code Coverage

![RunCodeCoverageExample](images/RunCodeCoverageExample.png "RunCodeCoverageExample")

The 'coverage' tab should appear:

![CoverageReportTab](images/CoverageReportTab.png "CoverageReportTab")

## Gradle Unit Test Report

![OpenUnitTestGeneratedReport01](images/OpenUnitTestGeneratedReport01.png "OpenUnitTestGeneratedReport01")

![OpenUnitTestGeneratedReport02](images/OpenUnitTestGeneratedReport02.png "OpenUnitTestGeneratedReport02")

## Extra
* Unit Test AAA Pattern: https://medium.com/@pjbgf/title-testing-code-ocd-and-the-aaa-pattern-df453975ab80
* Unit Test Method Naming Convention: https://dzone.com/articles/7-popular-unit-test-naming
* JUnit5 Cheat Sheet: https://junit.org/junit5/docs/current/user-guide/

# ==================================================
# Official Documentation
See: https://docs.gitlab.com/ee/ci/yaml/README.html

The documentation can be break down into 2 independent sections:
1. Global Keywords
2. Job Keywords

## 1. Global Keywords
### Stages
```shell
stages:
  - build
  - test
  - deploy
```
### Workflow
### Include

## 2. Job Keywords
| Keyword | Description |
|---------|-------------|
after_script	| Override a set of commands that are executed after job.
allow_failure	| Allow job to fail. A failed job does not cause the pipeline to fail.
artifacts	| List of files and directories to attach to a job on success.
before_script	| Override a set of commands that are executed before job.
cache	| List of files that should be cached between subsequent runs.
coverage	| Code coverage settings for a given job.
dependencies	| Restrict which artifacts are passed to a specific job by providing a list of jobs to fetch artifacts from.
environment	| Name of an environment to which the job deploys.
except	| Limit when jobs are not created.
extends	| Configuration entries that this job inherits from.
image	| Use Docker images.
include	| Include external YAML files.
inherit	| Select which global defaults all jobs inherit.
interruptible	| Defines if a job can be canceled when made redundant by a newer run.
needs	| Execute jobs earlier than the stage ordering.
only	| Limit when jobs are created.
pages	| Upload the result of a job to use with GitLab Pages.
parallel	| How many instances of a job should be run in parallel.
release	| Instructs the runner to generate a release object.
resource_group	| Limit job concurrency.
retry	| When and how many times a job can be auto-retried in case of a failure.
rules	| List of conditions to evaluate and determine selected attributes of a job, and whether or not it’s created.
script	| Shell script that is executed by a runner.
secrets	| The CI/CD secrets the job needs.
services	| Use Docker services images.
stage	| Defines a job stage.
tags	| List of tags that are used to select a runner.
timeout	| Define a custom job-level timeout that takes precedence over the project-wide setting.
trigger	| Defines a downstream pipeline trigger.
variables	| Define job variables on a job level.
when	| When to run job

## Example '.gitlab-ci.yml'
```yaml
default:
  image: alpine
  before_script:
    - echo "Executing 'before_script' before each job."
  after_script:
    - echo "Executing 'after_script' after each job."

stages:
  - build
  - test
  - deploy

my-build-job:
  stage: build
  before_script:
    - echo "Running 'before_script' inside 'my-build-job'."
  script:
    - echo "Running 'script' inside 'my-build-job'."
  after_script:
    - echo "Running 'after_script' inside 'my-build-job'."

my-test-job:
  stage: test
  before_script:
    - echo "Running 'before_script' inside 'my-test-job'."
  script:
    - echo "Running 'script' inside 'my-test-job'."
  after_script:
    - echo "Running 'after_script' inside 'my-test-job'."

my-deploy-job:
  stage: build
  before_script:
    - echo "Running 'before_script' inside 'my-deploy-job'."
  script:
    - echo "Running 'script' inside 'my-deploy-job'."
  after_script:
    - echo "Running 'after_script' inside 'my-deploy-job'."
```