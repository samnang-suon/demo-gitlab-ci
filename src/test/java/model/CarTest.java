package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CarTest {

    @Test
    void getBrand_ShouldReturnCorrectValue() {
        // Arrange
        Car car = new Car("TESLA", 4);

        // Act
        String returnValue = car.getBrand();

        // Assert
        assertEquals("TESLA", returnValue);
    }

    @Test
    void getDoors_ShouldReturnCorrectValue() {
        // Arrange
        Car car = new Car("TESLA", 4);

        // Act
        int returnValue = car.getDoors();

        // Assert
        assertEquals(4, returnValue);
    }

    @Test
    void setBrand_ShouldUpdateBrandValue() {
        // Arrange
        Car car = new Car("TESLA", 4);

        // Act
        String brand = "MASERATI";
        car.setBrand(brand);

        // Assert
        assertEquals(brand, car.getBrand());
    }

    @Test
    void setDoors_ShouldUpdateDoorsValue() {
        // Arrange
        Car car = new Car("TESLA", 4);

        // Act
        int doorNum = 2;
        car.setDoors(doorNum);

        // Assert
        assertEquals(doorNum, car.getDoors());
    }
}
