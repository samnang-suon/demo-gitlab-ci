package model;

public class Car {
    // Attribute(s)
    public String brand = null;
    public int doors = 0;

    // Constructor(s)
    public Car(
        String brand,
        int doors
    ) {
        this.brand = brand;
        this.doors = doors;
    }

    // Getter(s)
    public String getBrand() {
        return brand;
    }

    public int getDoors() {
        return doors;
    }

    // Setter(s)
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    // Other(s)
    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", doors=" + doors +
                '}';
    }
}
